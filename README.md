# GitLab

Hemos creado el repositorio en git lab con el nombre **Yesica_markdown**  

![imagen1](imagenes/a.PNG)  

Para tenerlo en local hemos clonado el repositorio en la carpeta donde queremos que este el repositorio con el comando "**cd /nombreDeLaCarpeta**" y luego hemos hecho "**git clone *La url del repositorio de gitlab***"

![imagen2](imagenes/b.PNG)  

Para crear una nueva carpeta hago click derecho y pulso nuevo/carpeta y le ponemos el nombre "privado", luego hago un click derecho y pulso nuevo/documento y le ponemos el nombre de "privada.txt".  

![imagen3](imagenes/c.PNG)


Para  que git te ignore los dos archivos privados tienes que crear un archivo que se llame "**.gitignore**" y dentro del indicar la ubicación de esos archivos. Git lo que hará es leer ese archivo  e ignorara todos aquellos archivos y carpetas que estén indicadas en su interior.

![imagen4](imagenes/d.PNG)

Como podemos observar esta ignorando la carpeta privado y el fichero ***"privada.txt"***

![imagen5](imagenes/e.PNG)  

creamos un nuevo fichero llamado yesica.md y que tenga una lista de las asignaturas en la que estoy matriculada.

![imagen6](imagenes/f.PNG)

Para hacer un tag tenemos que poner el comando "**git tag v0.1**"

![imagen7](imagenes/g.PNG)  

Para subir los cambios tenemos que hacer los procedimientos de siempre. **"git add ."**,
"**git commit -m "comimit de yesica"**", **"git push -u origin master"**

![imagen8](imagenes/h.PNG)  

|nombre|url|
|------|---|
|Henar|[url](https://gitlab.com/Henar/henar_markdown.git)|

*****

# HOJA03_MARKDOWN_03

1- Creacion de ramas

* Crea la rama rama-TUNOMBRE  

	Para crear la rama dentro de git bash debemos de usar el comando **"git branch rama-YESICA"**  

	![imagen9](imagenes/i.PNG) 
* Posiciona tu carpeta de trabajo en esta rama  

	Para posicionarme en ella tengo que usar el comando **"git checkout rama-YESICA"**

	![imagen10](imagenes/j.PNG) 

2-Añade un fichero y crea la rama remota

* Crea un fichero llamado daw.md con únicamente una cabecera DESARROLLO DE APLICACIONES WEB
	
	Para crear el fichero en la carpeta y le llamas daw.md
	![imagen11](imagenes/k.PNG) 
* Haz un commit con el mensaje “Añadiendo el archivo daw.md en la rama-TUNOMBRE”
	
	Para hacer el commit usamos los comandos **"git add .**,**git commit -m "Añadiendo el archivo daw.md en la rama-YESICA"**"

	![imagen11](imagenes/l.PNG) 

*	Sube los cambios al repositorio remoto. NOTA: date cuenta que ahora se deberá hacer con el comando **git push origin rama-YESICA**

	![imagen11](imagenes/m.PNG) 

3-Haz un merge directo  

*	Posiciónate en la rama master
	
	Para volver a la rama principal debemos poner **git checkout master** en este momento en la carpeta local, osea en la rama master no habra todo lo que hayas añadido en la rama-YESICA.

	![imagen11](imagenes/n.PNG) 

*	Haz un merge de la rama-TUNOMBRE en la rama master
	
	Para unir las dos ramas, se debe usar el comando **git merge rama-YESICA**. Despues de hacer el merge podemos ver que en nuestra carpeta local han aparecido todo lo que habiamos añadido en la rama-YESICA.

4-Haz un merge con conflicto  
*	En la rama master añade al fichero daw.md una tabla en la que muestres los módulos del primer curso de DAW.	  
	![imagen11](imagenes/o.PNG)

*	Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW1”
	
	Para hacer comit usamos los comandos de siempre **"git add .**,**git commit -m "Añadida tabla DAW1"**"

	![imagen11](imagenes/p.PNG)

*	Posiciónate ahora en la rama-TUNOMBRE
	
	Para ello usamos el comando **git checkout rama-YESICA**  

	![imagen11](imagenes/q.PNG)

*	Escribe en el fichero daw.md otra tabla con los módulos del segundo curso de DAW  

	![imagen11](imagenes/r.PNG)  

*	Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW2”

	![imagen11](imagenes/s.PNG)

*	Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE

	![imagen11](imagenes/t.PNG)

5-Arreglo del conflicto   
*	Arregla el conflicto editando el fichero daw.md y haz un commit con el mensaje "Finalizado el conflicto de daw.md"    
	Al hacer el merge y tener en ambas ramas cosas diferentes en las mismas lineas del documento daw.md habra un conflito, para solucionarlo tenemos que modificar el documento a mano  
	![imagen11](imagenes/t.PNG)
	![imagen11](imagenes/u.PNG)
	![imagen11](imagenes/v.PNG)
	![imagen11](imagenes/w.PNG)

6-Tag y borrar la rama  

*	Crea un tag llamado v0.2
	
	![imagen11](imagenes/z.PNG)

*	Borra la rama-TUNOMBRE	
	
	Para borrar la rama debemos usar el comando **git brach -d rama-YESICA**


	![imagen11](imagenes/y.PNG)

